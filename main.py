from fastapi import FastAPI
from pydantic import BaseModel
import openai

app = FastAPI(port=9015)

openai.api_key = "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"

class PreguntaIn(BaseModel):
    rol: str
    pregunta: str

class PreguntaOut(BaseModel):
    respuesta: str
    tokens_utilizados: int

@app.post("/preguntas")
async def generar_respuesta(pregunta_in: PreguntaIn):

    model = "gpt-3.5-turbo"
    max_tokens = 500

    texto_entrada = f"Rol: {pregunta_in.rol}\nPregunta: {pregunta_in.pregunta}"

    respuesta = openai.Completion.create(
        model=model,
        prompt=texto_entrada,
        max_tokens=max_tokens,
        log_level="info"
    )


    respuesta_generada = respuesta.choices[0].text.strip()
    tokens_utilizados = respuesta['usage']['total_tokens']

    print(respuesta_generada)
    return PreguntaOut(respuesta=respuesta_generada, tokens_utilizados=tokens_utilizados)
